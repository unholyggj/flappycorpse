﻿using UnityEngine;
using UnityEngine.UI;

public class SlotManager : MonoBehaviour
{
    [SerializeField] private Toggle[] _toggles;

    public void Init(ConfessionEnum conf)
    {
        foreach (var child in GetComponentsInChildren<HudSlot>())
        {
            child.Init(conf);
        }
    }

    public void Pick(int i)
    {
        _toggles[i].isOn = true;
    }

    public bool GetIsWin()
    {
        bool isWin = true;
        foreach (var toggle in _toggles)
        {
            if (!toggle.isOn) { isWin = false; }
        }
        return isWin;
    }
}