﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPair
{
	public PickupTypeEnum objType;
	public GameObject obj;
}

[System.Serializable]
public class CorpseModel
{
    public ConfessionEnum confession;
    public Renderer renderer;
}

public class Corpse : MonoBehaviour
{
    public GameObject powEffect;
    public ObjectPair[] stuff;

    public CorpseModel[] corpses;
    public God[] gods;

    public List<PickupTypeEnum> pickups;
    public PlayerData playerData;

    public Animator AnimationsAnim;
    public Animator EffectsAnim;

    public AudioSource audioSource;
    public AmbientSound ambientSound;
    public Backgrounds backgrounds;

    public bool started;
    private SlotManager _hudSlotManager;

    private void Start()
    {
        playerData = PlayerData.Generate();
        _hudSlotManager = FindObjectOfType<SlotManager>();
        _hudSlotManager.Init(playerData.Confession);

        foreach (var corpse in corpses) { corpse.renderer.gameObject.SetActive(corpse.confession == playerData.Confession); }
        if (ambientSound != null) { ambientSound.Play(playerData.Confession); }
        if (backgrounds != null) { backgrounds.SetBackground(playerData.Confession); }
    }

    public void StartGame()
    {
        GetComponent<CorpseMovement>().enabled = true;
        started = true;
    }

    private void OnTriggerEnter(Collider collider)              
    {
        if (collider.tag == "EndOfRitual") { Ending(); }

        var pickup = collider.GetComponentInChildren<Pickup>();
        if (pickup == null)
            return;

        Instantiate(powEffect, transform.position + 0.5f * transform.right, Quaternion.identity);
        collider.enabled = false;
        Debug.Log(pickup.type);
        var type = pickup.type;
        var conf = Pickup.GetConfession(type);

        // если не терминальный пикап
        if (!Pickup.IsEnd(type) && Pickup.GetConfession(type) == playerData.Confession)
            _hudSlotManager.Pick((Mathf.FloorToInt(((int)type )/ 3)));

        foreach (var park in stuff)
        {
            if (park.objType == type)
                park.obj.SetActive(true);
        }

        EffectsAnim.SetTrigger(type.ToString());

        if (pickup.onHitClip != null)
        {
            audioSource.clip = pickup.onHitClip;
            audioSource.Play();
        }
        if (pickup.animator != null)
        {
            pickup.animator.SetTrigger(type.ToString());
        }

        if (Pickup.IsEnd(type)) { Ending(); }

    }

    public void Ending()
    {
        if (ambientSound != null) { ambientSound.Stop(); }
        foreach (var god in gods)
        {
            god.ShowGod(playerData.Confession, _hudSlotManager.GetIsWin());
        }
        Destroy(GetComponent<CorpseMovement>());
        var m = FindObjectOfType<EndSceneManager>();
        m.PlayCutscene(this);
        var obsticles = FindObjectsOfType<FlyingPickup>();
        foreach (var obsticle in obsticles)
        {
            Destroy(obsticle.gameObject);
        }
    }

    public int CalcScore()
    {
        float rightCount = 0;
        if (pickups.Contains(playerData.TargetPickups[0]))
            rightCount++;
        if (pickups.Contains(playerData.TargetPickups[1]))
            rightCount++;
        if (pickups.Contains(playerData.TargetPickups[2]))
            rightCount++;

        return (int) (10f*rightCount/pickups.Count);
    }

    public void OnLastPickup()
    {
        var score = CalcScore();
        if (score == 10)
        {
            // все довольны
        }
        else
        {

        }
    }

    public void ThrowTo(Transform transform)
    {
        this.transform.SetParent(transform, true);
        this.transform.localRotation = Quaternion.identity;
        this.transform.localPosition = Vector3.zero;
    }
}