﻿public enum PickupTypeEnum
{
    None = -1,
    Tunic = 0,
    Armor = 1,
    Mummy = 2,

    Choir = 3,
    Fire = 4,
    Treasures = 5,

    Coffin = 6,
    Boat = 7,
    Sarcofag = 8,

    Chamber = 9,
    Sea = 10,
    Pyramid = 11,
}