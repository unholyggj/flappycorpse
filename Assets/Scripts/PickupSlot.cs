﻿using UnityEngine;

public class PickupSlot : MonoBehaviour
{
    [SerializeField] protected Vector2 _size;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, _size);
    }
}