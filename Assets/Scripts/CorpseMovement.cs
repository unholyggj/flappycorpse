﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CorpseMovement : MonoBehaviour
{
    enum State { Flying, Falling, Takeoff }

    public float speed = 5.0f;
    public float fallTime = 1.5f;
    public float flightLevel = 4.0f;
    public AnimationCurve curve;
	Vector3 rotationVector = Vector3.zero;
    public AudioSource groundHitAudio;

	Transform corpseTransform;

    State state { get; set; }

    void Awake()
    {
        ResetLevel();
		enabled = false;
		corpseTransform = GetComponentInChildren<Animator> ().transform;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && state != State.Falling)
        {
            StopAllCoroutines();
            Fall();
        }
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.forward * speed * Time.fixedDeltaTime);
		corpseTransform.Rotate (rotationVector * 2);
    }

    void Fall()
    {
        state = State.Falling;
        StartCoroutine(MoveVertical(-1.0f));
    }

    void Takeoff()
    {
		if (state == State.Falling)
			rotationVector = new Vector3 (Random.Range (0, 1f), Random.Range (0, 1f), Random.Range (0, 1f)).normalized;
        state = State.Takeoff;
        StartCoroutine(MoveVertical(1.0f));
    }

    void Fly()
    {
        state = State.Flying;
    }

    IEnumerator MoveVertical(float direction)
    {
        var body = GetComponent<Rigidbody>();
        var time = fallTime * ((direction + 1.0f) / 2.0f - (transform.position.y / flightLevel)) * direction;
        Debug.Log("TIME : " + time + "    Direction : " + direction);
        while (time >= 0)
        {
            yield return new WaitForEndOfFrame();
            time -= Time.deltaTime;
            var progress = ((direction + 1.0f) / 2.0f - (time / fallTime)) * direction;

            var position = transform.position;
            position.y = progress * flightLevel * curve.Evaluate(progress);
            transform.position = position;
        }
    }

    void ResetLevel()
    {
        var position = transform.position;
        position.y = flightLevel;
        transform.position = position;
    }

    void OnTriggerStay(Collider collider)
    {
        if (state != State.Falling) { return; }

        if (collider.gameObject.tag == "Ground")
        {
            Debug.Log(collider.name);
            StopAllCoroutines();
            Takeoff();

            if (groundHitAudio != null) { groundHitAudio.Play(); }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(Vector3.up * flightLevel, Vector3.left * 200.0f);
        Gizmos.DrawRay(Vector3.up * flightLevel, Vector3.right * 200.0f);
    }
}
