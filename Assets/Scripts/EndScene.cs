﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EndScene : MonoBehaviour
{
    [SerializeField] private ConfessionEnum _confession;
    [SerializeField] private Transform _corpseSlot;
    public ConfessionEnum Confession { get { return _confession; } }
    public System.Action<EndScene> EndingFinished;
    float sceneDuration = 3.5f;

    Camera mainCamera;
    Camera thisCamera;

    void Awake()
    {
        thisCamera = GetComponentInChildren<Camera>();
        mainCamera = Camera.main;
    }

    IEnumerator WaitEndOfAnimation()
    {
        var time = sceneDuration;
        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        EndFinish();
    }

    public void EndFinish()
    {
        Reset();
        if (EndingFinished != null) { EndingFinished(this); }
    }

    public void Play(Corpse corpse)
    {
        mainCamera.gameObject.SetActive(false);
        thisCamera.gameObject.SetActive(true);
        corpse.ThrowTo(_corpseSlot);
        _corpseSlot.GetComponent<Animator>().enabled = true;
        StartCoroutine(WaitEndOfAnimation());
    }

    public void Reset()
    {
        mainCamera.gameObject.SetActive(true);
        thisCamera.gameObject.SetActive(false);
        _corpseSlot.GetComponent<Animator>().enabled = false;
    }

}