﻿using System.Linq;
using CarrierShip;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Range(0f, 1f)] [SerializeField] private float _emptyProbability=0.2f;
    private const int EndGameOffset = (int)PickupTypeEnum.Chamber;
    [SerializeField] private GameObject _pickupPrefab;

    void Start()
    {
        var finalPickups = FindObjectsOfType<PickupSlot>().Where(s => !(s is PickupSlotGenerator)).Randomize().ToArray();
        for (var i = 0; i < finalPickups.Length; i++)
        {
            var p = finalPickups[i];
            var s= p.gameObject.AddComponent<PickupSwitcher>();
            var type = (PickupTypeEnum) i + EndGameOffset;
            s.Init(type, _pickupPrefab);
        }

        var gen = FindObjectOfType<PickupSlotGenerator>();
        var pickups = gen.Points.Randomize().ToArray();
        for (var i = 0; i < pickups.Length; i++)
        {
            if ((float)i/pickups.Length < _emptyProbability)
                continue;
            var g = new GameObject("Pickup");
            g.transform.SetParent(gen.transform, true);
            g.transform.position = pickups[i];
            var type = (PickupTypeEnum) (i % EndGameOffset);
            g.AddComponent<PickupSwitcher>().Init(type, _pickupPrefab);
            Debug.Assert(!Pickup.IsEnd(type));
            if (Pickup.IsEnd(type))
                Debug.Log("WTF");
        }
    }
}