﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    private const int ConfessionsCount = 3;
    [SerializeField] private Vector2 _size;
    [SerializeField] private PickupTypeEnum _type;
    public Animator animator;

    public AudioClip onHitClip;

    public PickupTypeEnum type { get { return _type; } }

    public Pickup Init(float pos)
    {
        return this;
    }

    public static ConfessionEnum GetConfession(PickupTypeEnum type)
    {
        return (ConfessionEnum)((int)type % ConfessionsCount);
    }

    public static bool IsEnd(PickupTypeEnum type)
    {
        switch (type)
        {
            case PickupTypeEnum.Chamber:
            case PickupTypeEnum.Sea:
            case PickupTypeEnum.Pyramid:
                return true;
        }
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, new Vector3(_size.x, _size.y, 1f));
    }
}