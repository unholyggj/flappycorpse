﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class God : MonoBehaviour {

    public Animator animator;
    public ConfessionEnum confession;

    public void ShowGod(ConfessionEnum conf, bool happiness)
    {
        animator.SetBool("isHere", conf == confession);
        animator.SetBool("isHappy", happiness);
    }

    public void HideGod()
    {
        animator.SetBool("isHere", false);
    }

}
