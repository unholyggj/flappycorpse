﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FlyingPickupSpawner : MonoBehaviour
{
    public CorpseMovement corpseMovement;
    public FlyingPickup[] pickups;
    public float ratePerMinute = 10.0f; // per minute
    public float disableDistance = 20.0f;

    List<FlyingPickup> freeList { get; set; }
    List<FlyingPickup> stagedList { get; set; }

    float duration { get; set; }
    float timer { get; set; }
    bool isPlaying { get; set; }

    void Awake()
    {
        freeList = new List<FlyingPickup>();
        stagedList = new List<FlyingPickup>();
        duration = 60.0f / ratePerMinute;
    }

	void Update()
    {
        if (!isPlaying) { return; }

        timer += Random.Range(0.0f, 2.0f * Time.deltaTime);
        if (timer > duration)
        {
            timer = 0.0f;
            Spawn();
        }

        stagedList = stagedList.FindAll(i => {
            bool disableItem = transform.position.x - i.transform.position.x > disableDistance;
            if (disableItem) {
                freeList.Add(i);
                i.gameObject.SetActive(false);
            }
            return !disableItem;
        });
    }

    public void Play()
    {
        isPlaying = true;
    }

    void Spawn()
    {
        var selectedProto = pickups[Random.Range(0, pickups.Length)];
        var cachedPickup = freeList.FirstOrDefault(i => i.attendedConfession == selectedProto.attendedConfession);

        if (cachedPickup == null)
        {
            cachedPickup = Instantiate(selectedProto);
        }

        var position = transform.position;
        position.y = corpseMovement.flightLevel;
        cachedPickup.transform.position = position;
        cachedPickup.gameObject.SetActive(true);

        stagedList.Add(cachedPickup);
    }

    public void Stop()
    {
        foreach (var pickup in stagedList)
        {
            pickup.gameObject.SetActive(false);
            freeList.Add(pickup);
        }
        stagedList.Clear();
    }
}
