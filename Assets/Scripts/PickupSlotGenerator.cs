﻿using System.Collections.Generic;
using UnityEngine;

public class PickupSlotGenerator : PickupSlot
{
    [SerializeField] protected float _distance;
    [SerializeField] private Transform _endPoint;
    [SerializeField] private Vector2[] _points;
    public Vector2[] Points { get { return _points; } }

    public void GeneratePoints()
    {
        var res = new List<Vector2>();
        var y = transform.position.y;

        for (var x = transform.position.x; x < _endPoint.position.x; x += _distance)
            res.Add(new Vector2(x, y));

        _points = res.ToArray();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        foreach (var point in Points)
            Gizmos.DrawWireCube(point, _size);
    }
}