﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Bounds bounds;
    public float damping = 4.0f;

	public AnimationCurve wobbleX;
	public AnimationCurve wobbleY;
	public AnimationCurve wobbleZ;
	float time;
	Vector3 realPosition;

	Corpse corpse;

	void Awake ()
	{
		realPosition = transform.position;
		corpse = GameObject.FindWithTag ("Player").GetComponent<Corpse>();
	}

    void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    void FixedUpdate()
    {
        var localTarget = transform.InverseTransformPoint(target.position);
		time += Time.fixedDeltaTime;
		time %= 1;
		var wobble = new Vector3 (wobbleX.Evaluate (time), wobbleY.Evaluate (time), wobbleZ.Evaluate (time));
	
        if (bounds.SqrDistance(localTarget) > 0.0f)
        {
            var delta = localTarget - bounds.ClosestPoint(localTarget);

			realPosition = Vector3.Lerp(realPosition, realPosition + delta, damping * Time.fixedDeltaTime);
        }

		transform.position = realPosition;
		if (corpse.started)
			transform.position += wobble;

    }
}
