﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace CarrierShip
{
    public static class Extensions
    {
        public static T InstantiateAsChild<T>(this Component parent, T prefab) where T:Component
        {
            var p = Object.Instantiate(prefab);
            if (parent == null) return p;
            var t = p.transform;
            t.SetParent(parent.transform, false);
            return p;
        }

        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> src)
        {
            return src.OrderBy(_ => Random.Range(0f, 1f));
        }
    }
}