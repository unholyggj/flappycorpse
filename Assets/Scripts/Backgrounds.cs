﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Backgrounds : MonoBehaviour
{

    [System.Serializable]
    public class Background
    {
        public ConfessionEnum confession;
        public GameObject background;
    }

    public Background[] backgrounds = {};

    public void SetBackground(ConfessionEnum confession)
    {
        foreach (var background in backgrounds)
        {
            background.background.SetActive(background.confession == confession);
        }
    }
}