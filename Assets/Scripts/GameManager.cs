﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text startLabel;
    public GameObject pressToStartImage;
    private bool _started;

    void Start () {
		
	}
	
	void Update () {
		if (!_started && Input.GetKeyUp(KeyCode.Space)){
            pressToStartImage.gameObject.SetActive(false);
            StartCoroutine (GameStarter ());
        }
	}

	IEnumerator GameStarter ()
	{
	    _started = true;
		for (int i = 3; i >= 1; i--) {
			startLabel.text = i.ToString ();
			yield return new WaitForSeconds (0.5f);
		}
		startLabel.enabled = false;
		GameObject.FindWithTag ("Player").GetComponent<Corpse>().StartGame();
	}
}
