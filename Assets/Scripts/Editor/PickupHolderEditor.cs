﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (PickupSlotGenerator))]
public class PickupHolderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Generate"))
            ((PickupSlotGenerator)target).GeneratePoints();
    }
}