﻿using UnityEngine;
using System.Collections;

public class FlyingPickup : MonoBehaviour
{
    public ConfessionEnum attendedConfession;

    public Vector3 speed = Vector3.left;

    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }
}
