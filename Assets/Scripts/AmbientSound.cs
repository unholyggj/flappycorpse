﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class AmbientSound : MonoBehaviour
{
    public AmbientRecord[] ambients = {};
    public AmbientRecord[] endings = { };
    public AudioSource whistleAudioSource;
    ConfessionEnum confession;
    float fadeTime = 0.7f;
    AudioSource source { get; set; }

    [System.Serializable]
    public class AmbientRecord
    {
        public ConfessionEnum confession;
        public AudioClip clip;
    }

	void Awake()
    {
        source = GetComponent<AudioSource>();
	}
	
    public void Play(ConfessionEnum confession)
    {
        this.confession = confession;
        var record = ambients.FirstOrDefault(i => i.confession == confession);
        if (record == null)
        {
            source.Stop();
        }
        else
        {
            source.clip = record.clip;
            source.Play();
        }
    }

    public void Stop()
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        var time = fadeTime;
        if(whistleAudioSource != null) { whistleAudioSource.Play(); }
        while (time >= 0.0f)
        {
            time -= Time.deltaTime;
            source.volume = time / fadeTime;
            yield return new WaitForEndOfFrame();
        }
        source.Stop();
        var record = endings.FirstOrDefault(i => i.confession == confession);
        if (record != null)
        {
            source.clip = record.clip;
            source.Play();
            time = source.clip.length;
            while (time >= 0.0f)
            {
                time -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            source.Stop();
        }
    }
}
