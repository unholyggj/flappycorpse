﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    public ConfessionEnum Confession { get; private set; }
    public PickupTypeEnum[] TargetPickups;

    public static PlayerData Generate()
    {
        var conf = (ConfessionEnum) Random.Range(0, 3);
        var pickups = new List<PickupTypeEnum>();

        for (var i = (PickupTypeEnum) conf; i <= PickupTypeEnum.Pyramid; i += 3)
            pickups.Add(i);

        return new PlayerData
        {
            Confession = conf,
            TargetPickups = pickups.ToArray()
        };
    }
}