﻿using CarrierShip;
using UnityEngine;

public class PickupSwitcher : MonoBehaviour
{
    public PickupTypeEnum PickupType { get; private set; }

    public void Init(PickupTypeEnum type, GameObject prefab)
    {
        PickupType = type;
        var t = transform.InstantiateAsChild(prefab.transform);
        foreach (var pickup in t.GetComponentsInChildren<Pickup>())
            pickup.gameObject.SetActive(pickup.type == PickupType);
    }
}