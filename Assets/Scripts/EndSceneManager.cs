﻿using System.Linq;
using UnityEngine;

public class EndSceneManager : MonoBehaviour
{
    public EndScene[] _scenes;

    private void Awake()
    {
        foreach (var scene in _scenes)
            scene.EndingFinished += SceneFinished;
        ResetEndScenes();
    }

    void OnDisable()
    {
        foreach (var scene in _scenes)
            scene.EndingFinished -= SceneFinished;
    }

    void SceneFinished(EndScene end)
    {
        //TODO Refactor and restart normally, restart movers, corpse, etc
        UnityEngine.SceneManagement.SceneManager.LoadScene(Application.loadedLevel);
    }

    public void ResetEndScenes()
    {
        foreach (var scene in _scenes)
            scene.gameObject.SetActive(false);
    }

    public EndScene Get(ConfessionEnum confession)
    {
        return _scenes.FirstOrDefault(s => s.Confession == confession);
    }

    public void PlayCutscene(Corpse corpse)
    {
        var endCamera = Get(corpse.playerData.Confession);
        endCamera.gameObject.SetActive(true);
        endCamera.Play(corpse);
    }
}