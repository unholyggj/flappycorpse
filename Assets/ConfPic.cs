﻿using System;
using UnityEngine;

[Serializable]
public class ConfPic
{
    public ConfessionEnum Conf;
    public Sprite Sprite;
}