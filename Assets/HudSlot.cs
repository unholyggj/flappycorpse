﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HudSlot : MonoBehaviour
{
    public ConfPic[] pics = {new ConfPic {Conf = ConfessionEnum.Christ}, new ConfPic() {Conf = ConfessionEnum.Nord}, new ConfPic() {Conf = ConfessionEnum.Egypt}, };

    public void Init(ConfessionEnum c)
    {
        var t = GetComponent<Image>();
        t.sprite = pics.FirstOrDefault(p => p.Conf == c).Sprite;
    }
}